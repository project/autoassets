<?php

/**
 * @file
 * Hooks description file.
 */

/**
 * Implements hook_autoassets_prepare_modifiers_alter().
 */
function hook_autoassets_prepare_modifiers_alter(&$suggestions) {
  if ($term = menu_get_object('taxonomy_term', 2)) {
    $modifier = $term->name;
    $modifier = drupal_html_class($modifier);
    $modifier = trim($modifier, AUTOASSETS_DELIMITER);
    array_unshift($suggestions, $modifier);
  }
}
