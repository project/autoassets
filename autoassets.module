<?php

/**
 * @file
 * Contents of Auto Assets module.
 *
 * @see https://www.drupal.org/docs/7/theming/overview-of-theme-files
 */

/**
 * Weights of the groups.
 */
define('AUTOASSETS_HTML', 0);
define('AUTOASSETS_PAGE', 1);
define('AUTOASSETS_REGION', 2);
define('AUTOASSETS_ENTITY', 100);
define('AUTOASSETS_BLOCK', 999);

/**
 * Delimiter for the file names.
 */
define('AUTOASSETS_DELIMITER', '-');

/**
 * Saves a single suggestion.
 *
 * @param string $suggestion
 *   Suggestion code.
 * @param int $group
 *   Group.
 * @param int $weight
 *   Weight.
 */
function autoassets_add_suggestion($suggestion, $group = AUTOASSETS_BLOCK, $weight = 0) {
  $suggestions = &drupal_static(__FUNCTION__, array());
  $suggestions["$group"][$suggestion] = $weight;
}

/**
 * Saves the suggestions based on suggestion modifiers.
 *
 * @param string $base
 *   Base name for the suggestion.
 * @param int $group
 *   Group.
 * @param string $directory
 *   Directory to assign the suggestion to.
 */
function autoassets_set_suggestions($base, $group, $directory = '') {
  if ($modifiers = autoassets_get_modifiers(arg(), $base)) {
    foreach ($modifiers as $weight => $modifier) {
      autoassets_add_suggestion(trim($directory . '/' . $modifier, '/'), $group, $weight);
    }
  }
}

/**
 * Gets file name modifiers.
 *
 * @param mixed $args
 *   List of arguments.
 * @param string $base
 *   Base prefix for modifiers.
 * @param string $delimiter
 *   Delimiter.
 *
 * @return array
 *   Array of modifiers.
 */
function autoassets_get_modifiers($args, $base, $delimiter = AUTOASSETS_DELIMITER) {
  $modifiers = autoassets_prepare_modifiers($args, $delimiter);

  foreach ($modifiers as &$modifier) {
    $modifier = trim($base . $delimiter . $modifier, $delimiter);
  }

  return $modifiers;
}

/**
 * Prepares file name modifiers.
 *
 * @param mixed $args
 *   List of arguments.
 * @param string $delimiter
 *   Delimiter.
 *
 * @return array
 *   Array of prepared modifiers.
 */
function autoassets_prepare_modifiers($args, $delimiter = AUTOASSETS_DELIMITER) {
  $suggestions = &drupal_static(__FUNCTION__);

  if (empty($suggestions)) {
    // Suggestions.
    $base = '';
    $suggestions = theme_get_suggestions($args, $base, $delimiter);
    drupal_alter('autoassets_prepare_modifiers', $suggestions);
    array_unshift($suggestions, $base);

    // Prepare suggestions.
    array_walk($suggestions, function (&$suggestion) {
      $suggestion = drupal_html_class($suggestion);
      $suggestion = trim($suggestion, AUTOASSETS_DELIMITER);
    });
    $suggestions = array_unique($suggestions);
  }

  return $suggestions;
}

/**
 * Implements hook_flush_caches().
 */
function autoassets_flush_caches() {
  if (variable_get('autoassets_scan_flush_caches', 1)) {
    autoassets_scan();
  }
  return array();
}

/**
 * Static list of supported extensions.
 *
 * @return array
 *   List of extensions.
 */
function autoassets_extensions() {
  return array('css', 'js');
}

/**
 * Returns the list of theme subdirectories to scan for files.
 *
 * @param string $theme_name
 *   Theme name.
 * @param string $ext
 *   Extension from autoassets_extensions().
 *
 * @return array
 *   List of directories relatively to the theme path.
 */
function autoassets_theme_directories($theme_name, $ext) {
  $directories = variable_get('autoassets_theme_' . $ext . '__' . $theme_name, $ext);
  return explode(';', $directories);
}

/**
 * Scans the directories for the suggestion files.
 *
 * @param string $theme_name
 *   Theme name.
 */
function autoassets_scan($theme_name = NULL) {
  if (empty($theme_name)) {
    $theme_name = variable_get('theme_default', '');
  }
  $theme_path = drupal_get_path('theme', $theme_name);

  $scan = array();
  foreach (autoassets_extensions() as $ext) {
    $scan[$ext] = array();

    foreach (autoassets_theme_directories($theme_name, $ext) as $directory) {
      $scandir = DRUPAL_ROOT . '/' . $theme_path . '/' . $directory;
      $files = file_scan_directory($scandir, '@\.' . $ext . '$@');
      foreach ($files as $file => $file_info) {
        $file = substr($file, strlen(DRUPAL_ROOT . '/'));

        // Get suggestion.
        $suggestion = substr($file, strlen($theme_path . '/' . $directory . '/'));
        $suggestion = substr($suggestion, 0, -strlen('.' . $ext));

        // Save.
        $scan[$ext] += array($suggestion => array());
        $scan[$ext][$suggestion][] = $file;
      }
    }
  }

  variable_set('autoassets_scan:' . $theme_name, $scan);
}

/**
 * Implements hook_preprocess_html().
 */
function autoassets_preprocess_html(&$variables) {
  global $theme;

  autoassets_set_suggestions('html', AUTOASSETS_HTML);

  // Suggestions.
  $suggestions = drupal_static('autoassets_add_suggestion', array());

  // Alter.
  drupal_alter('autoassets_add_suggestion', $suggestions);
  ksort($suggestions);

  // Development.
  if (variable_get('autoassets_devel', FALSE)) {
    $theme_path = drupal_get_path('theme', $theme);

    $paths = array_fill_keys(autoassets_extensions(), array());
    foreach ($suggestions as $group => $files) {
      asort($suggestions[$group]);
      foreach ($suggestions[$group] as $suggestion => $weight) {
        foreach (autoassets_extensions() as $ext) {
          foreach (autoassets_theme_directories($theme, $ext) as $directory) {
            $paths[$ext][] = $theme_path . '/' . $directory . '/' . $suggestion;
          }
        }
      }
    }

    drupal_add_js(array('autoassets' => $paths), 'setting');
  }

  // Deliver assets to the page.
  $scan = variable_get('autoassets_scan:' . $theme, array());
  foreach (autoassets_extensions() as $ext) {
    $paths = array();

    foreach ($suggestions as $group => $files) {
      // Existing files.
      $files = array_intersect_key($files, $scan[$ext]);
      asort($files);

      // Add to the page.
      foreach ($files as $suggestion => $weight) {
        if (!empty($scan[$ext][$suggestion])) {
          foreach ($scan[$ext][$suggestion] as $path) {
            $paths[] = $path;
          }
        }
      }
    }

    // Alter list of files.
    $_ext = $ext;
    drupal_alter('autoassets', $paths, $_ext);

    // Add to the page.
    foreach ($paths as $path) {
      switch ($ext) {
        case 'css':
          drupal_add_css($path);
          break;

        case 'js':
          drupal_add_js($path);
          break;
      }
    }

    // Output loaded files in header for reference.
    if (variable_get('autoassets_html_head', FALSE)) {
      $markup = '';
      $markup .= '<script type="text/javascript">';
      $markup .= sprintf('var %s = %s;', 'autoassets_loaded_' . $ext, json_encode($paths));
      $markup .= '</script>';
      $element = array(
        '#type' => 'markup',
        '#weight' => -999,
        '#markup' => $markup,
      );
      drupal_add_html_head($element, 'autoassets_html_head_' . $ext);
    }
  }
}

/**
 * Implements hook_preprocess_page().
 */
function autoassets_preprocess_page(&$variables) {
  autoassets_set_suggestions('page', AUTOASSETS_PAGE, 'page');

  // Node.
  if ($node = menu_get_object()) {
    autoassets_set_suggestions('node', AUTOASSETS_ENTITY, 'node');
    autoassets_set_suggestions('node--' . $node->type, AUTOASSETS_ENTITY, 'node');
  }

  // Term.
  if ($term = menu_get_object('taxonomy_term', 2)) {
    autoassets_set_suggestions('term', AUTOASSETS_ENTITY, 'term');
    autoassets_set_suggestions('term--' . $term->vocabulary_machine_name, AUTOASSETS_ENTITY, 'term');
  }
}

/**
 * Implements hook_autoassets_prepare_modifiers_alter().
 */
function autoassets_autoassets_prepare_modifiers_alter(&$modifiers) {
  // Node.
  if ($node = menu_get_object()) {
    array_unshift($modifiers, 'nodetype-' . $node->type);
  }

  // Term.
  if ($term = menu_get_object('taxonomy_term', 2)) {
    array_unshift($modifiers, 'vocabulary-' . $term->vocabulary_machine_name);
  }
}

/**
 * Implements hook_preprocess_region().
 */
function autoassets_preprocess_region(&$variables) {
  autoassets_set_suggestions('region--' . $variables['region'], AUTOASSETS_REGION, 'region');
}

/**
 * Implements hook_preprocess_block().
 */
function autoassets_preprocess_block(&$variables) {
  $block = $variables['block'];

  autoassets_set_suggestions('block', AUTOASSETS_BLOCK, 'block');
  autoassets_set_suggestions('block--' . $block->module, AUTOASSETS_BLOCK, 'block');
  autoassets_set_suggestions('block--' . $block->module . '--' . $block->delta, AUTOASSETS_BLOCK, 'block');

  // Views.
  if ('views' === $block->module) {
    $delta = $block->delta;

    // Hashes.
    if (strlen($delta) == 32) {
      $hashes = variable_get('views_block_hashes', array());
      if (!empty($hashes[$delta])) {
        $delta = $hashes[$delta];
      }
    }

    // Get view name and display id.
    $delta_split = explode('-', $delta, 2);
    if (!empty($delta_split[1])) {
      // Load view.
      list($view_name, $display_id) = $delta_split;
      $view = views_get_view($view_name);
      $view->set_display($display_id);

      // Views.
      autoassets_views_pre_view($view, $display_id, array());
    }
  }
}

/**
 * Implements hook_views_pre_view().
 */
function autoassets_views_pre_view($view, $display_id, $args) {
  autoassets_set_suggestions('view', AUTOASSETS_BLOCK, 'views');
  autoassets_set_suggestions('view--' . $view->name, AUTOASSETS_BLOCK, 'views');
  autoassets_set_suggestions('view--' . $view->name . '--' . $display_id, AUTOASSETS_BLOCK, 'views');

  // View Mode.
  if (isset($view->display[$display_id]->handler)) {
    $handler = $view->display[$display_id]->handler;
    if ('node' === $handler->get_option('row_plugin')) {
      if ($row_options = $handler->get_option('row_options')) {
        if (!empty($row_options['view_mode'])) {
          // Synthetic node.
          $node = new stdClass();
          $node->type = '';

          // Styles.
          autoassets_node_view($node, $row_options['view_mode'], LANGUAGE_NONE);
        }
      }
    }
  }
}

/**
 * Implements hook_preprocess_menu_tree().
 */
function autoassets_preprocess_menu_tree(&$variables) {
  $menu_name = substr($variables['theme_hook_original'], strlen('menu_tree__'));
  autoassets_set_suggestions('menu', AUTOASSETS_BLOCK, 'menu');
  autoassets_set_suggestions('menu--' . $menu_name, AUTOASSETS_BLOCK, 'menu');
}

/**
 * Implements hook_node_view().
 */
function autoassets_node_view($node, $view_mode, $langcode) {
  autoassets_set_suggestions('viewmode--' . $view_mode, AUTOASSETS_BLOCK, 'ds');
  autoassets_set_suggestions('viewmode--' . $view_mode . '--' . $node->type, AUTOASSETS_BLOCK, 'ds');
}
